import java.io.FileInputStream;
import java.io.FileOutputStream;

public class fileinputoutputdemo {
    public static void main(String[] args) {
        try{
            FileInputStream fin =new FileInputStream("input.txt");
            FileOutputStream fout = new FileOutputStream("output.txt");
            int i=0;
            while((i=fin.read()) != -1){
                fout.write(i);
            }
            fout.close();
            fin.close();
            System.out.println("success");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
